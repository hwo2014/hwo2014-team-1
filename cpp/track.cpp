#include "track.h"
#include "math.h"
#include "piece.h"
#include "car.h"
#include <cmath>
#include <iostream>
#include <functional>
#include <cassert>

track::track()
: m_id()
, m_name()
, m_pieces()
, m_lanes()
, m_startX(0.0)
, m_startY(0.0)
, m_startAngle(0.0)
{

}

track::track(const std::string& id,
             const std::string& name,
             const std::vector<piece*>& pieces,
             const std::vector<lane*>& lanes,
             double startX,
             double startY,
             double startAngle)
: m_id(id)
, m_name(name)
, m_pieces(pieces)
, m_lanes(lanes)
, m_startX(startX)
, m_startY(startY)
, m_startAngle(startAngle)
{
    updateRoute(0, 0, std::map<std::string, car*>());
}

track::~track()
{
    for (auto it = m_pieces.begin(); it != m_pieces.end(); it++)
    {
        delete *it;
        *it = nullptr;
    }
    for (auto it = m_lanes.begin(); it != m_lanes.end(); it++)
    {
        delete *it;
        *it = nullptr;
    }
}

void track::updateRoute(int pieceIndex,
                        int startLane,
                        const std::map<std::string, car*>& cars)
{
    if (m_pieces.empty()) return;

    int numberOfSwitches = 0;
    for (auto it = m_pieces.begin(); it != m_pieces.end(); it++)
    {
        const piece* p = *it;
        if (p->hasSwitch()) numberOfSwitches++;
    }
    if (numberOfSwitches <= 0) return;

    // utility funcs
    auto nextPieceIndex = [] (int pieceIndex, std::vector<piece*>::size_type max)
    {
        return ((pieceIndex + 1) >= max) ? 0 : (pieceIndex + 1);
    };

    piece* p = m_pieces[pieceIndex];

    while (!p->hasSwitch())
    {
        pieceIndex = nextPieceIndex(pieceIndex, m_pieces.size());
        p = m_pieces[pieceIndex];
    }
//    std::cout << "From piece " << pieceIndex << " to";
    std::vector<lane*>::size_type leftLane = static_cast<decltype(leftLane)>(std::max<int>(0, startLane - 1));
    std::vector<lane*>::size_type rightLane = std::min<decltype(rightLane)>(m_lanes.size() - 1, startLane + 1);
    double leftDistance = p->distance(m_lanes[startLane], m_lanes[leftLane]);
    double rightDistance = p->distance(m_lanes[startLane], m_lanes[rightLane]);

    pieceIndex = nextPieceIndex(pieceIndex, m_pieces.size());
    piece *np = m_pieces[pieceIndex];

    while (!np->hasSwitch())
    {
        leftDistance += np->distance(m_lanes[leftLane], m_lanes[leftLane]);
        rightDistance += np->distance(m_lanes[rightLane], m_lanes[rightLane]);
        pieceIndex = nextPieceIndex(pieceIndex, m_pieces.size());
        np = m_pieces[pieceIndex];
    }

    if (leftDistance < rightDistance)
    {
        p->setBestLane(leftLane);
    }
    else
    {
        p->setBestLane(rightLane);
    }
}

piece* track::getPiece(int pieceIndex) const
{
    if (pieceIndex >= 0 &&
        pieceIndex < m_pieces.size())
    {
        return m_pieces[pieceIndex];
    }
    return nullptr;
}

std::vector<piece*>::size_type track::numPieces() const
{
    return m_pieces.size();
}

lane* track::getLane(int laneIndex) const
{
    if (laneIndex >= 0 &&
        laneIndex < m_lanes.size())
    {
        return m_lanes[laneIndex];
    }
    return nullptr;
}

std::vector<lane*>::size_type track::numLanes() const
{
    return m_lanes.size();
}

int track::ticksToCorner(int pieceIndex,
                         double pd,
                         int nextPieceIndex,
                         double speed,
                         int startLane,
                         int endLane) const
{
    if (speed < 0.0001) return 10;

    piece* p = m_pieces[pieceIndex];
    piece* np = m_pieces[nextPieceIndex];

    double d = p->distance(m_lanes[startLane], m_lanes[endLane]);

    int ticks = 0;

    d -= pd;

    while ((d - speed) > 0.0)
    {
        d -= speed;
        ticks++;
    }

    if (p == np)
    {
        return ticks;
    }

    int nextStartLane = endLane;
    int nextEndLane = endLane;
    std::vector<piece*>::size_type lastIndex = (nextPieceIndex <= pieceIndex) ? (m_pieces.size() + nextPieceIndex) : nextPieceIndex;
    for (std::vector<piece*>::size_type i = pieceIndex + 1; i <= lastIndex; i++)
    {
        std::vector<piece*>::size_type ri = (i >= m_pieces.size()) ? (i - m_pieces.size()) : i;

        piece* np = m_pieces[ri];

        std::vector<lane*>::size_type nextBestLane = np->bestLane();

        if (nextBestLane != -1 &&
            nextBestLane != nextEndLane)
        {
            nextStartLane = nextEndLane;
            if (nextBestLane > nextEndLane) nextEndLane++;
            if (nextBestLane < nextEndLane) nextEndLane--;
        }

        d += np->distance(m_lanes[nextStartLane], m_lanes[nextEndLane]);

        while (d > 0.0)
        {
            d -= speed;
            ticks++;
        }
    }

    return ticks;
}
