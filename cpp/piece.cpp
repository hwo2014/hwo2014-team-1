#include "piece.h"
#include "lane.h"
#include "math.h"
#include <iostream>
#include <cmath>

// PIECE

piece::piece()
: m_switch(false)
, m_bestLane(-1)
, m_switchingLane(false)
{
    
}

piece::piece(bool hasSwitch)
: m_switch(hasSwitch)
, m_bestLane(-1)
, m_switchingLane(false)
{

}

piece::~piece()
{

}

bool piece::hasSwitch() const
{
    return m_switch;
}

std::vector<lane*>::size_type piece::bestLane() const
{
    return m_bestLane;
}

void piece::setBestLane(std::vector<lane*>::size_type bestLane)
{
    m_bestLane = bestLane;
}

bool piece::switchingLane() const
{
    return m_switchingLane;
}

void piece::setSwitchingLane(bool switching)
{
    m_switchingLane = switching;
}

// STRAIGHT PIECE

straight_piece::straight_piece()
: piece(false)
{

}

straight_piece::straight_piece(bool hasSwitch,
                               double length)
: piece(hasSwitch)
, m_length(length)
{

}

straight_piece::~straight_piece()
{

}

piece::Type straight_piece::type() const
{
    return piece::STRAIGHT;
}

double straight_piece::distance(const lane* ls,
                                const lane* le)
{
    if (ls && le)
    {
        if (hasSwitch() &&
            ls->distanceFromCenter() != le->distanceFromCenter())
        {
            double switchDistance = fabs(ls->distanceFromCenter()) + fabs(le->distanceFromCenter());
            double switchLength = m_length * 0.0403;
            switchDistance = sqrt(switchLength * switchLength + switchDistance * switchDistance);
            double s = sqrt(m_length * m_length + switchDistance * switchDistance);
            return s;
        }
    }
    return m_length;
}

double straight_piece::length() const
{
    return m_length;
}

// CURVE PIECE

curve_piece::curve_piece()
: piece(false)
{

}

curve_piece::curve_piece(bool hasSwitch,
                         double radius,
                         double angle)
: piece(hasSwitch)
, m_radius(radius)
, m_angle(angle)
, m_distance(0.0)
, m_distanceLS(-1)
, m_distanceLE(-1)
{

}

curve_piece::~curve_piece()
{

}

piece::Type curve_piece::type() const
{
    return piece::CURVE;
}

double curve_piece::distance(const lane* ls,
                             const lane* le)
{
    if (m_distanceLS == ls->index() &&
        m_distanceLE == le->index())
    {
        return m_distance;
    }

    const double angleRad = math::DEGTORAD * fabs(m_angle);
    const double angleSign = math::sign(m_angle);
    if (ls && le)
    {
        double s = 0.0;
        if (hasSwitch() &&
            ls->distanceFromCenter() != le->distanceFromCenter())
        {
            const double loLimit = std::min(ls->distanceFromCenter(), le->distanceFromCenter());
            const double hiLimit = std::max(ls->distanceFromCenter(), le->distanceFromCenter());
            const int iterations = 50;
            const double step = angleRad / double(iterations - 1);
//            std::cout << std::endl;
            for (int i = 0; i < iterations - 1; i++)
            {
                double startAngle = step * double(i);
                double endAngle = step * double(i + 1);
                double t1 = std::min(1.0, startAngle / angleRad);
                double t2 = std::min(1.0, endAngle / angleRad);
                double d1u = angleSign * math::lerp(ls->distanceFromCenter(), le->distanceFromCenter(), t1);
                double d2u = angleSign * math::lerp(ls->distanceFromCenter(), le->distanceFromCenter(), t2);
                double d1 = std::min(hiLimit, std::max(loLimit, d1u));
                double d2 = std::min(hiLimit, std::max(loLimit, d2u));
                double x1 = cos(startAngle) * (m_radius - d1);
                double y1 = sin(startAngle) * (m_radius - d1);
                double x2 = cos(endAngle) * (m_radius - d2);
                double y2 = sin(endAngle) * (m_radius - d2);
                double dx = x2 - x1;
                double dy = y2 - y1;
//                std::cout << "d1u, " << d1u << ", d2u, " << d2u << ", x1, " << x1 << ", y1, " << y1 << ", x2, " << x2 << ", y2, " << y2 << std::endl;
                s += sqrt(dx * dx + dy * dy);
            }
            return s;
        }
        s = angleRad * (m_radius - angleSign * ls->distanceFromCenter());
        m_distanceLS = ls->index();
        m_distanceLE = le->index();
        m_distance = s;
        return s;
    }
    return angleRad * m_radius;
}

double curve_piece::radius() const
{
    return m_radius;
}

double curve_piece::angle() const
{
    return m_angle;
}
