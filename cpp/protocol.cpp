#include "protocol.h"

namespace hwo_protocol
{

    jsoncons::json make_request(const std::string& msg_type,
                                const jsoncons::json& data,
                                int gameTick)
    {
//        std::cout << "make_request(" << msg_type << ", " << data << ", " << gameTick << ")" << std::endl;
        jsoncons::json r;
        r["msgType"] = msg_type;
        r["data"] = data;
        if (gameTick != -1) r["gameTick"] = gameTick;
        return r;
    }

    jsoncons::json make_join(const std::string& name,
                             const std::string& key)
    {
        jsoncons::json data;
        data["name"] = name;
        data["key"] = key;
        return make_request("join", data);
    }

    jsoncons::json make_ping()
    {
        return make_request("ping", jsoncons::null_type());
    }

    jsoncons::json make_throttle(double throttle,
                                 int gameTick)
    {
        return make_request("throttle", throttle, gameTick);
    }

    jsoncons::json make_turbo()
    {
        return make_request("turbo", "Paremmalla eelle!");
    }
    
}  // namespace hwo_protocol
