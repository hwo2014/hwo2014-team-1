#pragma once

#include <string>
#include <vector>

class lane;

class piece
{
public:
    enum Type
    {
        STRAIGHT,
        CURVE
    };

public:
    piece(bool hasSwitch);
    virtual ~piece();

    virtual Type type() const = 0;
    virtual double distance(const lane* ls,
                            const lane* le) = 0;

    bool hasSwitch() const;
    std::vector<lane*>::size_type bestLane() const;
    void setBestLane(std::vector<lane*>::size_type bestLane);

    bool switchingLane() const;
    void setSwitchingLane(bool switching);

private:
    piece();
    
private:
    bool                            m_switch;
    std::vector<lane*>::size_type   m_bestLane;
    bool                            m_switchingLane;
};

class straight_piece : public piece
{
public:
    straight_piece(bool hasSwitch,
                   double length);
    virtual ~straight_piece();

    Type type() const;
    double distance(const lane* ls,
                    const lane* le);

    double length() const;

private:
    straight_piece();

private:
    double  m_length;
};

class curve_piece : public piece
{
public:
    curve_piece(bool hasSwitch,
                double radius,
                double angle);
    virtual ~curve_piece();

    Type type() const;
    double distance(const lane* ls,
                    const lane* le);

    double radius() const;
    double angle() const;

private:
    curve_piece();
    
private:
    double  m_radius;
    double  m_angle;
    double  m_distance;
    int     m_distanceLS;
    int     m_distanceLE;
};