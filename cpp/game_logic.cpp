#include "game_logic.h"
#include "protocol.h"
#include "math.h"

using namespace hwo_protocol;

game_logic::game_logic()
: action_map
{
    { "join", &game_logic::on_join },
    { "yourCar", &game_logic::on_your_car },
    { "gameInit", &game_logic::on_game_init },
    { "gameStart", &game_logic::on_game_start },
    { "carPositions", &game_logic::on_car_positions },
    { "turboAvailable", &game_logic::on_turbo_available },
    { "turboStart", &game_logic::on_turbo_start },
    { "turboEnd", &game_logic::on_turbo_end },
    { "crash", &game_logic::on_crash },
	{ "spawn", &game_logic::on_spawn },
    { "lapFinished", &game_logic::on_lap_finished },
    { "dnf", &game_logic::on_dnf },
    { "finish", &game_logic::on_finish },
    { "gameEnd", &game_logic::on_game_end },
    { "error", &game_logic::on_error },
    { "createRace", &game_logic::on_create_race },
    { "joinRace", &game_logic::on_join_race },
}
, m_cars()
, m_myCar(nullptr)
, m_track(nullptr)
, m_gameTick(-1)
, m_init(false)
, m_started(false)
, m_myCarInit(false)
{
    std::cout << std::fixed << std::setprecision(5);
}

game_logic::~game_logic()
{
    for (auto it = m_cars.begin(); it != m_cars.end(); it++)
    {
        delete it->second;
        it->second = nullptr;
    }
    m_myCar = nullptr;
    delete m_track;
    m_track = nullptr;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
//    for (auto it = msg.begin_members(); it != msg.end_members(); it++)
//    {
//        std::cout << it->name() << " = " << it->value() << std::endl;
//    }
    if (msg.has_member("gameTick"))
    {
        if (m_gameTick != -1 ||
            msg["msgType"].as<std::string>().compare("gameStart") == 0)
        {
            if (m_gameTick != msg["gameTick"].as<int>())
            {
                m_gameTick = msg["gameTick"].as<int>();
//                std::cout << msg["msgType"] << " gameTick: " << m_gameTick << std::endl;
            }
        }
    }
    std::string msg_type = msg["msgType"].as<std::string>();
	const jsoncons::json& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
	{
		return (action_it->second)(this, data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	std::cout << "My car" << " name: " << data["name"] << " color: " << data["color"] << std::endl;
    m_myCar = new car(true);
    m_cars.insert(std::make_pair(data["name"].as<std::string>(), m_myCar));
    m_myCarInit = true;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "Game init" << std::endl;

    // load track
    const jsoncons::json& trackData = data["race"]["track"];
    std::vector<piece*> pieces;
    pieces.resize(trackData["pieces"].size());
    int pieceIndex = 0;
    for (auto it2 = trackData["pieces"].begin_elements(); it2 != trackData["pieces"].end_elements(); it2++)
    {
        const jsoncons::json& pieceData = *it2;
        bool hasSwitch = false;
        if (pieceData.has_member("switch"))
        {
            hasSwitch = pieceData["switch"].as<bool>();
        }
        if (pieceData.has_member("length"))
        {
            pieces[pieceIndex++] = new straight_piece(hasSwitch, pieceData["length"].as<double>());
        }
        else if(pieceData.has_member("radius") &&
                pieceData.has_member("angle"))
        {
            pieces[pieceIndex++] = new curve_piece(hasSwitch, pieceData["radius"].as<double>(), pieceData["angle"].as<double>());
        }
    }
    std::vector<lane*> lanes;
    lanes.resize(trackData["lanes"].size());
    for (auto it2 = trackData["lanes"].begin_elements(); it2 != trackData["lanes"].end_elements(); it2++)
    {
        const jsoncons::json& laneData = *it2;
        lanes[laneData["index"].as<int>()] = new lane(laneData["distanceFromCenter"].as<int>(), laneData["index"].as<int>());
    }
    m_track = new track(trackData["id"].as<std::string>(),
                        trackData["name"].as<std::string>(),
                        pieces,
                        lanes,
                        trackData["startingPoint"]["position"]["x"].as<double>(),
                        trackData["startingPoint"]["position"]["y"].as<double>(),
                        trackData["startingPoint"]["angle"].as<double>());

    // load cars
    std::string name;
    const jsoncons::json& cars = data["race"]["cars"];
    for (auto it = cars.begin_elements(); it != cars.end_elements(); it++)
    {
        const jsoncons::json& carData = *it;
        name = carData["id"]["name"].as<std::string>();
        if (m_cars.find(name) == m_cars.end())
        {
            m_cars.insert(std::make_pair(name, new car(false)));
        }
        m_cars[name]->setup(name,
                            carData["id"]["color"].as<std::string>(),
                            carData["dimensions"]["length"].as<double>(),
                            carData["dimensions"]["width"].as<double>(),
                            carData["dimensions"]["guideFlagPosition"].as<double>(),
                            trackData["startingPoint"]["angle"].as<double>());
    }

    m_init = true;

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;
    m_started = true;
    return { make_throttle(1.0, m_gameTick) };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    if (!m_init ||
        !m_started ||
        !m_myCarInit)
    {
        return { make_ping() };
    }

    jsoncons::json action;
    std::string name;
    int pieceIndex = -1;
    double piecePosition = 0.0;
    int startLane = 0;
    int endLane = 0;
    piece* currentPiece = nullptr;
    piece* nextPiece = nullptr;
    for (auto it = data.begin_elements(); it != data.end_elements(); it++)
    {
        const jsoncons::json& posData = *it;
        name = posData["id"]["name"].as<std::string>();
        pieceIndex = posData["piecePosition"]["pieceIndex"].as<int>();
        piecePosition = posData["piecePosition"]["inPieceDistance"].as<double>();
        startLane = posData["piecePosition"]["lane"]["startLaneIndex"].as<int>();
        endLane = posData["piecePosition"]["lane"]["endLaneIndex"].as<int>();
        currentPiece = m_track->getPiece(pieceIndex);
        nextPiece = m_track->getPiece(pieceIndex + 1);
        std::string pieceType = (currentPiece->type() == piece::STRAIGHT) ? (currentPiece->hasSwitch() ? "ss" : "sn") : (currentPiece->hasSwitch() ? "cs" : "cn");
        double curveAngle = (currentPiece->type() == piece::CURVE) ? static_cast<curve_piece*>(currentPiece)->angle() : 0.0;
        double pieceLength = currentPiece->distance(m_track->getLane(startLane), m_track->getLane(endLane));
        m_cars[name]->update(posData["angle"].as<double>(),
                             pieceIndex,
                             pieceLength,
                             piecePosition,
                             posData["piecePosition"]["lap"].as<int>(),
                             startLane,
                             endLane,
                             m_gameTick);
        m_track->updateRoute(pieceIndex,
                             startLane,
                             m_cars);
        if (m_myCar &&
            m_myCar->name() == name)
        {
            std::cout << "tick " << m_gameTick << ", pc, " << pieceIndex << ", pos, " << piecePosition << ", pt, " << pieceType << ", ca, " << curveAngle << ", acc, " << m_myCar->acceleration() << ", spe, " << m_myCar->speed() << ", ang, " << m_myCar->angle();

            if (nextPiece &&
                nextPiece->bestLane() != -1 &&
                !nextPiece->switchingLane())
            {
                if (nextPiece->bestLane() > startLane)
                {
                    if (action.type() == jsoncons::json::empty_object_t) action = make_request("switchLane", "Right");
                    nextPiece->setSwitchingLane(true);
                }
                else if (nextPiece->bestLane() < startLane)
                {
                    if (action.type() == jsoncons::json::empty_object_t) action = make_request("switchLane", "Left");
                    nextPiece->setSwitchingLane(true);
                }
            }

            // utility funcs
            auto nextPieceIndex = [] (int pieceIndex, std::vector<piece*>::size_type max)
            {
                return ((pieceIndex + 1) >= max) ? 0 : (pieceIndex + 1);
            };
            
            double straightDistance = 0.0;
            int pi = nextPieceIndex(pieceIndex, m_track->numPieces());
            piece* p = m_track->getPiece(pi);
            while (p && p->type() == piece::STRAIGHT)
            {
                straightDistance += p->distance(nullptr, nullptr);
                pi = nextPieceIndex(pi, m_track->numPieces());
                p = m_track->getPiece(pi);
            }

            if (m_myCar->turboState() == car::TURBO_AVAILABLE &&
                straightDistance >= 300 &&
                m_myCar->angle() <= 30.0)
            {
                if (action.type() == jsoncons::json::empty_object_t) action = make_turbo();
                m_myCar->setTurbo(car::TURBO_ACTIVE);
            }

        }
    }

    double throttle = m_myCar->throttle(m_track);
    std::cout << ", th, " << throttle << std::endl;
    if (action.type() == jsoncons::json::empty_object_t) action = make_throttle(throttle, m_gameTick);

//    for (auto it = action.begin_members(); it != action.end_members(); it++)
//    {
//        std::cout << it->name() << " = " << it->value() << std::endl;
//    }

	return { action };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
    std::cout << "Turbo available" << " durms: " << data["turboDurationMilliseconds"] << " durticks: " << data["turboDurationTicks"] << " factor: " << data["turboFactor"] << std::endl;
    m_myCar->setTurbo(car::TURBO_AVAILABLE,
                      data["turboDurationMilliseconds"].as<double>(),
                      data["turboDurationTicks"].as<int>(),
                      data["turboFactor"].as<double>());
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data)
{
    std::cout << "Turbo start" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data)
{
    std::cout << "Turbo end" << std::endl;
    m_myCar->setTurbo(car::TURBO_UNAVAILABLE);
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Someone crashed" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::cout << "Spawned" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
	std::cout << "Lap finished" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data)
{
	std::cout << "Did not finish" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	std::cout << "Finished" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.as<std::string>() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_create_race(const jsoncons::json& data)
{
	std::cout << "Race created" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_join_race(const jsoncons::json& data)
{
	std::cout << "Race joined" << std::endl;
	return { make_ping() };
}
