#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <map>
#include "piece.h"
#include "lane.h"

class car;

class track
{
public:
    track(const std::string& id,
          const std::string& name,
          const std::vector<piece*>& pieces,
          const std::vector<lane*>& lanes,
          double startX,
          double startY,
          double startAngle);

    virtual ~track();

    void updateRoute(int pieceIndex,
                     int startLane,
                     const std::map<std::string, car*>& cars);

    piece* getPiece(int pieceIndex) const;
    std::vector<piece*>::size_type numPieces() const;

    lane* getLane(int laneIndex) const;
    std::vector<lane*>::size_type numLanes() const;

    int ticksToCorner(int pieceIndex,
                      double pd,
                      int nextPieceIndex,
                      double speed,
                      int startLane,
                      int endLane) const;

private:
    track();

private:
    std::string                         m_id;
    std::string                         m_name;
    std::vector<piece*>                 m_pieces;
    std::vector<lane*>                  m_lanes;
    double                              m_startX;
    double                              m_startY;
    double                              m_startAngle;

};
