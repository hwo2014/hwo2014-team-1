#pragma once

class lane
{
public:
    lane(int distanceFromCenter,
         int index);
    virtual ~lane();

    int distanceFromCenter() const;
    int index() const;

private:
    lane();
private:
    int m_distanceFromCenter;
    int m_index;
};