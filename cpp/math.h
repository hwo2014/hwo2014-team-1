#pragma once

namespace math
{

    const double PI = 3.14159265;
    const double DEGTORAD = (PI / 180.0);
    const double RADTODEG = (180.0 / PI);

    // sign
    
    // -- overload unsigned value passed
    template<class T>
    inline T sign(T value, std::false_type)
    {
        return (T(0) < value);
    }

    // -- overload signed value passed
    template<class T>
    inline T sign(T value, std::true_type)
    {
        return (T(0) < value) - (value < T(0));
    }

    template<class T>
    inline T sign(T value)
    {
        return sign(value, std::is_signed<T>());
    }

    // --linear
    inline float lerp(float v1, float v2, float t)
    {
        // defined only with t[0, 1]
        return ((v2 - v1) * t + v1);
    }

    // --ease in out cubic
    inline double easeInOutCubic(double v1, double v2, double t)
    {
        // defined only with t[0, 1]
        t *= 2.0f;
        if (t < 1.0f)
        {
            return (v2 - v1) * 0.5f * t * t * t + v1;
        }
        t -= 2.0f;
        return (v2 - v1) * 0.5f * (t * t * t + 2.0f) + v1;
    }

};