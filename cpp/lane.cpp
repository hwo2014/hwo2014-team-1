#include "lane.h"

lane::lane()
{

}

lane::lane(int distanceFromCenter,
           int index)
: m_distanceFromCenter(distanceFromCenter)
, m_index(index)
{

}

lane::~lane()
{

}

int lane::distanceFromCenter() const
{
    return m_distanceFromCenter;
}

int lane::index() const
{
    return m_index;
}