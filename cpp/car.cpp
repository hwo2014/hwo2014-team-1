#include "car.h"
#include "piece.h"
#include "track.h"
#include "math.h"
#include <iostream>
#include <cmath>

car::car()
: m_my(false)
, m_lastAngle(0.0)
, m_angle(0.0)
, m_pieceIndex(0)
, m_pieceLength(0.0)
, m_lastPieceDistance(0.0)
, m_pieceDistance(0.0)
, m_lap(0)
, m_startLane(0)
, m_endLane(0)
, m_gameTick(0)
, m_deltaTick(0)
, m_name()
, m_color()
, m_width(0.0)
, m_length(0.0)
, m_guideFlagPosition(0.0)
, m_turboState(car::TURBO_UNAVAILABLE)
, m_turboDurationMilliseconds(0.0)
, m_turboDurationTicks(0)
, m_turboFactor(0.0)
, m_speed(0.0)
, m_lastSpeed(0.0)
, m_acceleration(0.0)
, m_firstAcceleration(0.0)
, m_lastThrottle(0.0)
{
}

car::car(bool my)
: m_my(my)
, m_lastAngle(0.0)
, m_angle(0.0)
, m_pieceIndex(0)
, m_pieceLength(0.0)
, m_lastPieceDistance(0.0)
, m_pieceDistance(0.0)
, m_lap(0)
, m_startLane(0)
, m_endLane(0)
, m_gameTick(0)
, m_deltaTick(0)
, m_name()
, m_color()
, m_width(0.0)
, m_length(0.0)
, m_guideFlagPosition(0.0)
, m_turboState(car::TURBO_UNAVAILABLE)
, m_turboDurationMilliseconds(0.0)
, m_turboDurationTicks(0)
, m_turboFactor(0.0)
, m_speed(0.0)
, m_lastSpeed(0.0)
, m_acceleration(0.0)
, m_firstAcceleration(0.0)
, m_lastThrottle(0.0)
{
}

void car::setup(const std::string& name,
                const std::string& color,
                double length,
                double width,
                double guideFlagPosition,
                double angle)
{
    m_name = name;
    m_color = color;
    m_length = length;
    m_width = width;
    m_guideFlagPosition = guideFlagPosition;
    m_lastAngle = angle;
    m_angle = angle;
}

void car::update(double angle,
                 int pieceIndex,
                 double pieceLength,
                 double pieceDistance,
                 int lap,
                 int startLane,
                 int endLane,
                 int gameTick)
{
    m_deltaTick = (gameTick - m_gameTick);
    if (m_pieceIndex != pieceIndex)
    {
        m_lastPieceDistance = m_pieceDistance - m_pieceLength;
    }
    else
    {
        m_lastPieceDistance = m_pieceDistance;
    }
    m_lastAngle = m_angle;
    m_angle = angle;
    m_pieceIndex = pieceIndex;
    m_pieceLength = pieceLength;
    m_pieceDistance = pieceDistance;
    m_lap = lap;
    m_startLane = startLane;
    m_endLane = endLane;
    m_lastSpeed = m_speed;
    m_speed = (m_pieceDistance - m_lastPieceDistance) / m_deltaTick;
    m_acceleration = (m_speed - m_lastSpeed);
    if (m_lastSpeed == 0.0 &&
        m_speed != 0.0)
    {
        m_firstAcceleration = m_acceleration;
    }
    m_gameTick = gameTick;
}

const std::string& car::name() const
{
    return m_name;
}

double car::angle() const
{
    return m_angle;
}

double car::throttle(const track* track)
{
    if (m_speed == 0.0)
    {
        m_lastThrottle = 1.0;
    }
    else
    {

        // utility funcs
        auto nextPieceIndex = [] (int pieceIndex, std::vector<piece*>::size_type max)
        {
            return ((pieceIndex + 1) >= max) ? 0 : (pieceIndex + 1);
        };

        piece* p = track->getPiece(m_pieceIndex);
        piece* np = nullptr;

        int pi = nextPieceIndex(m_pieceIndex, track->numPieces());
        np = track->getPiece(pi);

        if (p->type() == piece::CURVE)
        {
            np = p;
        }
        else
        {
            while(np &&
                  np->type() != piece::CURVE)
            {
                pi = nextPieceIndex(pi, track->numPieces());
                np = track->getPiece(pi);
            }
        }

        m_lastThrottle = 1.0;

        if (m_speed > 0.0)
        {
            const double maxSlipAngle = (math::DEGTORAD  * 59.5);
            const double throttleStep = 0.001;
            const int ticksForward = (m_speed * m_speed) / 3;
            double slipAngle = predictSlipAngle(ticksForward, m_speed, track, m_startLane, m_endLane, maxSlipAngle);
            std::cout << ", pi, " << pi << ", sa0, " << slipAngle << ", ttc0, " << ticksForward;
            double speed = m_speed;
            while (fabs(slipAngle) > maxSlipAngle && m_lastThrottle > 0.0)
            {
                m_lastThrottle = std::max(0.0, m_lastThrottle - throttleStep);
                speed = predictSpeed(ticksForward, m_lastThrottle);
                slipAngle = predictSlipAngle(ticksForward, speed, track, m_startLane, m_endLane, maxSlipAngle);
            }
            slipAngle = predictSlipAngle(1, m_speed, track, m_startLane, m_endLane, maxSlipAngle);
            std::cout << ", spp, " << speed <<  ", psa, " << math::RADTODEG * slipAngle;
        }
    }

    return m_lastThrottle;
}

car::TurboState car::turboState() const
{
    return m_turboState;
}

void car::setTurbo(car::TurboState state,
                   double durationMilliseconds,
                   int durationTicks,
                   double factor)
{
    m_turboState = state;
    if (m_turboState == car::TURBO_AVAILABLE ||
        m_turboState == car::TURBO_UNAVAILABLE)
    {
        m_turboDurationMilliseconds = durationMilliseconds;
        m_turboDurationTicks = durationTicks;
        m_turboFactor = factor;
    }
}

double car::speed() const
{
    return m_speed;
}

double car::lastSpeed() const
{
    return m_lastSpeed;
}

double car::magic() const
{
    return 0.02;
}

double car::acceleration() const
{
    return m_acceleration;
}

double car::firstAcceleration() const
{
    return m_firstAcceleration;
}

double car::predictAcceleration(double speed,
                                double throttle) const
{
    return m_firstAcceleration * throttle * ((m_turboState == car::TURBO_ACTIVE) ? m_turboFactor : 1.0) - speed * magic();
}

double car::predictSpeed(int numTicks,
                         double throttle) const
{
    double speed = m_speed;
    for (int i = 0; i < numTicks; i++)
    {
        speed += predictAcceleration(speed, throttle);
    }
    return speed;
}

double car::predictSlipAngle(int numTicks,
                             double speed,
                             const track* track,
                             int startLane,
                             int endLane,
                             double maxSlipAngle) const
{
    const lane* ls = track->getLane(startLane);
    const lane* le = track->getLane(endLane);
    const double speed2 = speed * speed;
    const double halfPI = math::PI * 0.5;
    const double epsilon = 0.00001;
    const double friction = 5;

    // utility funcs
    auto nextPieceIndex = [] (int pieceIndex, std::vector<piece*>::size_type max)
    {
        return ((pieceIndex + 1) >= max) ? 0 : (pieceIndex + 1);
    };

    double angle = math::DEGTORAD * m_angle;
    double pos = m_pieceDistance;
    int pi = m_pieceIndex;
    piece* p = track->getPiece(pi);
    double aC = 0.0;
    double vy = 0.0;

    for (int i = 0; i < numTicks; i++)
    {
//        std::cout << "tick, " << i;
        if (p->type() == piece::CURVE)
        {
            curve_piece* cp = static_cast<curve_piece*>(p);
            const double loLimit = std::min(ls->distanceFromCenter(), le->distanceFromCenter());
            const double hiLimit = std::max(ls->distanceFromCenter(), le->distanceFromCenter());
            const double angleRad = math::DEGTORAD * cp->angle();
            const double angleSign = math::sign(cp->angle());
            const double curAngle = std::min(angleRad, angleRad * (pos / cp->distance(ls, le)));
//            std::cout << ", ca, " << curAngle;
            if (cp->hasSwitch() &&
                ls->distanceFromCenter() != le->distanceFromCenter())
            {
                double t = std::min(1.0, curAngle / angleRad);
                double du = angleSign * math::lerp(ls->distanceFromCenter(), le->distanceFromCenter(), t);
                double d = std::min(hiLimit, std::max(loLimit, du));
                aC = speed2 / (cp->radius() - d);
            }
            else
            {
                double d = angleSign * ls->distanceFromCenter();
                aC = speed2 / (cp->radius() - d);
            }
        }
        else
        {
            aC = 0.0;
        }
        vy += aC;
        double rvy = vy;
        if ((fabs(rvy) - friction) < 0.0)
        {
            rvy = 0.0;
        }
        double slipAngle = -atan(std::max(-halfPI + epsilon, std::min(halfPI - epsilon, rvy / speed)));
        angle += slipAngle;
//        std::cout << ", pc, " << pi << ", pos, " << pos << ", aC, " << aC << ", vy, " << vy << ", ang, " << angle << ", sp, " << speed << ", sp2, " << speed2 << ", sad, " << slipAngle << std::endl;
        if (fabs(angle) > maxSlipAngle)
        {
            break;
        }
        pos += speed;
        int pd = p->distance(ls, le);
        if (pos > pd)
        {
            pos -= pd;
            pi = nextPieceIndex(pi, track->numPieces());
            p = track->getPiece(pi);
        }
    }
    return angle;
}

int car::pieceIndex() const
{
    return m_pieceIndex;
}

int car::startLane() const
{
    return m_startLane;
}

int car::endLane() const
{
    return m_endLane;
}

double car::pieceDistance() const
{
    return m_pieceDistance;
}

bool car::my() const
{
    return m_my;
}