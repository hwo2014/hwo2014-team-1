#pragma once

#include <string>

class track;

class car
{
public:
    enum TurboState
    {
        TURBO_UNAVAILABLE,
        TURBO_AVAILABLE,
        TURBO_ACTIVE,
    };
public:
    car(bool my);

    void setup(const std::string& name,
               const std::string& color,
               double length,
               double width,
               double guideFlagPosition,
               double angle);

    void update(double angle,
                int pieceIndex,
                double pieceLength,
                double pieceDistance,
                int lap,
                int startLane,
                int endLane,
                int gameTick);

    const std::string& name() const;

    double angle() const;

    double throttle(const track* track);

    TurboState turboState() const;
    void setTurbo(TurboState state,
                  double durationMilliseconds = 0.0,
                  int durationTicks = 0,
                  double factor = 0.0);

    double speed() const;
    double lastSpeed() const;
    double magic() const;
    double acceleration() const;
    double firstAcceleration() const;
    double predictAcceleration(double speed,
                               double throttle) const;
    double predictSpeed(int numTicks,
                        double throttle) const;
    double predictSlipAngle(int numTicks,
                            double speed,
                            const track* track,
                            int startLane,
                            int endLane,
                            double maxSlipAngle) const;

    int pieceIndex() const;
    double pieceDistance() const;

    int startLane() const;
    int endLane() const;

    bool my() const;

private:
    car();

private:
    bool        m_my;
    double      m_lastAngle;
    double      m_angle;
    int         m_pieceIndex;
    double      m_pieceLength;
    double      m_lastPieceDistance;
    double      m_pieceDistance;
    int         m_lap;
    int         m_startLane;
    int         m_endLane;
    int         m_gameTick;
    int         m_deltaTick;
    double      m_length;
    double      m_width;
    double      m_guideFlagPosition;
    std::string m_color;
    std::string m_name;
    TurboState  m_turboState;
    double      m_turboDurationMilliseconds;
    int         m_turboDurationTicks;
    double      m_turboFactor;
    double      m_lastSpeed;
    double      m_speed;
    double      m_acceleration;
    double      m_firstAcceleration;
    double      m_lastThrottle;
};